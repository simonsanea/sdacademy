package SDA.sdacademy.JavaAdvancedFeaturesExercises.Collection.Companie;

public class Persoane {
    private String name;

    public Persoane(String name) {
        this.name = name;
    }

    public Persoane() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
