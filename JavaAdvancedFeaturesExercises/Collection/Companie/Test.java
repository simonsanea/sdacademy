package SDA.sdacademy.JavaAdvancedFeaturesExercises.Collection.Companie;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Persoane ion = new Somer("Ion");
        Persoane alex = new Somer("Alex");
        List<Persoane> person = new ArrayList<>();
        person.add(ion);
        person.add(alex);
        System.out.println(person);

        Persoane vasile = new Angajat("Vasile");
        Persoane tudor = new Angajat("Tudor");
        person.add(vasile);
        person.add(tudor);
        System.out.println(person);

        Persoane petrea = new Student("Petrea");
        Persoane andrea = new Student("Andrea");
        person.add(petrea);
        person.add(andrea);
        System.out.println(person);

        for (Persoane persoane : person) {
            System.out.println(persoane.getName());
        }
        System.out.println(person.get(2).getName());
        System.out.println(person.indexOf(tudor));
    }
}
