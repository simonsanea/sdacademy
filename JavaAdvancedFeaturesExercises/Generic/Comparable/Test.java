package SDA.sdacademy.JavaAdvancedFeaturesExercises.Generic.Comparable;

public class Test {
    public static void main(String[] args) {
        Person andrew = new Person(12);
        Person alex = new Person(16);

        if (andrew.compareTo(alex)>0){
            System.out.println("Alex is taller");
        }
        else {
            System.out.println("Andrew is smaller");
        }
    }
}
