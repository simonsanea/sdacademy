package SDA.sdacademy.JavaAdvancedFeaturesExercises.Generic.Comparable;

public class Person implements Comparable<Person> {
    private int height;

    public Person(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int compareTo(Person otherHeight) {
        return this.height-otherHeight.height;
    }
}
