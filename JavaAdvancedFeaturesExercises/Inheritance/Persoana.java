package SDA.sdacademy.JavaAdvancedFeaturesExercises.Inheritance;

public class Persoana {
    private int virsta;
    private String nume;
    private String cnp;
    private Adresa adresa;

    public Persoana(int virsta, String nume, String cnp, Adresa adresa) {
        this.virsta = virsta;
        this.nume = nume;
        this.cnp = cnp;
        this.adresa = adresa;
    }

    public Persoana() {

    }

    public Persoana(int virsta, String nume, String cnp, Adresa adresa, Somer somer) {
        this.virsta = virsta;
        this.nume = nume;
        this.cnp = cnp;
        this.adresa = adresa;
    }

    public int getVirsta() {
        return virsta;
    }

    public void setVirsta(int virsta) {
        this.virsta = virsta;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public Adresa getAdresa() {
        return adresa;
    }

    public void setAdresa(Adresa adresa) {
        this.adresa = adresa;
    }

    public int getNrOreSaptamanaLucrate() {
        return 0;
    }
}
