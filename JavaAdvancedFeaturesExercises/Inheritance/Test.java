package SDA.sdacademy.JavaAdvancedFeaturesExercises.Inheritance;

public class Test {
    public static void main(String[] args) {
        Tara romania = new Tara("Romania", "RO", true);
        Companie SDA = new Companie("SDA", romania);
        Adresa adresa = new Adresa(romania, "Iasi", "12345");
        Angajat angajat = new Angajat(36, "Ion Popescu", "123456", adresa, SDA, 1000);

        System.out.println(angajat.getNume() + " este din orasul " + angajat.getAdresa().getOras());
    }
}
