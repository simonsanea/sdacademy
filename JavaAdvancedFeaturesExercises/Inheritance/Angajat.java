package SDA.sdacademy.JavaAdvancedFeaturesExercises.Inheritance;

public class Angajat extends Persoana {
    private Companie companie;
    private int salariu;

    public Angajat(int virsta, String nume, String cnp, Adresa adresa, Companie companie, int salariu) {
        super(virsta, nume, cnp, adresa);
        this.companie = companie;
        this.salariu = salariu;
    }

    public Angajat() {

    }

    public Companie getCompanie() {
        return companie;
    }

    public void setCompanie(Companie companie) {
        this.companie = companie;
    }

    public int getSalariu() {
        return salariu;
    }

    public void setSalariu(int salariu) {
        this.salariu = salariu;
    }

    @Override
    public int getNrOreSaptamanaLucrate() {
        return 40;
    }
}
