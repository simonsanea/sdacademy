package SDA.sdacademy.JavaAdvancedFeaturesExercises.Inheritance;

public class Somer extends Persoana {
    private boolean timpLiber;
    private String meserieBaza;

    public Somer(int virsta, String nume, String cnp, Adresa adresa, boolean timpLiber, String meserieBaza) {
        super(virsta, nume, cnp, adresa);
        this.timpLiber = timpLiber;
        this.meserieBaza = meserieBaza;
    }

    public Somer() {

    }

    public boolean isTimpLiber() {
        return timpLiber;
    }

    public void setTimpLiber(boolean timpLiber) {
        this.timpLiber = timpLiber;
    }

    public String getMeserieBaza() {
        return meserieBaza;
    }

    public void setMeserieBaza(String meserieBaza) {
        this.meserieBaza = meserieBaza;
    }
}
