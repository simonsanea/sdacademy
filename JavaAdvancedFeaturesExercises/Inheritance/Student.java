package SDA.sdacademy.JavaAdvancedFeaturesExercises.Inheritance;

public class Student extends Persoana {
    private Facultate facultate;
    private int anStudiu;

    public Student(int virsta, String nume, String cnp, Adresa adresa, Facultate facultate, int anStudiu) {
        super(virsta, nume, cnp, adresa);
        this.facultate = facultate;
        this.anStudiu = anStudiu;
    }

    public Student() {

    }

    public Facultate getFacultate() {
        return facultate;
    }

    public void setFacultate(Facultate facultate) {
        this.facultate = facultate;
    }

    public int getAnStudiu() {
        return anStudiu;
    }

    public void setAnStudiu(int anStudiu) {
        this.anStudiu = anStudiu;
    }

    @Override
    public int getNrOreSaptamanaLucrate() {
        return 8;
    }
}
