package SDA.sdacademy.JavaFundamentalsExercises.StringExercises;

import java.util.Scanner;

public class Text {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String text = in.nextLine();
        System.out.println(text.toLowerCase());
    }
}
