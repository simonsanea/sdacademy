package SDA.sdacademy.JavaAdvancedFeatures.OOPExercises.Animals.cats;

import SDA.sdacademy.JavaAdvancedFeatures.OOPExercises.Animals.dogs.Animal;

public class Cat extends Animal {

    @Override
    public void run(){
        System.out.println("Cat is running");
    }

    /** COMPILE ERROR: sleep() has default access modifier. Animal class is not in the same package with Cat
    @Override
    void sleep(){
        System.out.println("Dog is sleeping");
    }
    */

    @Override
    public void beHappy(){
        System.out.println("The cat is happy");
    }
}
