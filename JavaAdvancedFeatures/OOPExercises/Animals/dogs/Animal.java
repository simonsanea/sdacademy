package SDA.sdacademy.JavaAdvancedFeatures.OOPExercises.Animals.dogs;

public class Animal {
    public final int age = 10;

    void sleep(){
        System.out.println("Animal is sleeping");
    }

    public static void eat(){
        System.out.println("Animal is eating");
    }

    public static void eat(String animal){
        System.out.println("Animal " + animal + " is eating");
    }

    protected void run(){
        System.out.println("Animal is running");
    }

    protected void beHappy(){
        System.out.println("Animals is happy");
    }
}
