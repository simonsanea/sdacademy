package SDA.sdacademy.JavaAdvancedFeatures.OOPExercises.Animals.dogs;

public class Dog extends Animal {

    public static void eat(){
        System.out.println("Dog is eating");
    }

    public static void eat(int kilograms, String animal){

    }

    @Override
    public void run(){
        System.out.println("Dog is running");
    }

    @Override
    void sleep(){
        System.out.println("Dog is sleeping");
    }

    @Override
    protected void beHappy(){
        System.out.println("THe dog is happy");
    }
}
