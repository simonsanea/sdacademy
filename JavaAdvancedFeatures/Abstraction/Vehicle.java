package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public abstract class Vehicle {
    private int maxSpeed;

    public Vehicle(int maxSpeed){
        this.maxSpeed = maxSpeed;
    }
    public Vehicle(){}

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public abstract void move();
}
