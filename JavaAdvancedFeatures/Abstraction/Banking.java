package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public abstract class Banking {

    public abstract void pay();
}
