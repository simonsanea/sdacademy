package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public class MaestroCard extends Banking {
    @Override
    public void pay() {
        System.out.println("Payment has been done with a Maestro card");
    }
}
