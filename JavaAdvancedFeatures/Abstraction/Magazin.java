package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public class Magazin {
    private Banking banking;

    public Magazin(Banking banking){
        this.banking = banking;
    }

    public Magazin(){}

    public Banking getBanking() {
        return banking;
    }

    public void setBanking(Banking banking) {
        this.banking = banking;
    }
}
