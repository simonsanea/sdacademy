package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public class Main {
    public static void main(String[] args) {
        Banking cardMastercard = new MasterCard();
        Banking cardVisa = new VisaCard();
        Banking maestro = new MaestroCard();

        Magazin magazin = new Magazin(maestro);
        magazin.getBanking().pay();


    }
}
