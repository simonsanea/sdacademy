package SDA.sdacademy.JavaAdvancedFeatures.Abstraction;

public class VisaCard extends Banking {
    @Override
    public void pay() {
        System.out.println("Payment has been done with a Visa card");
    }
}
