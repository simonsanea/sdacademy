package SDA.sdacademy.JavaAdvancedFeatures.Exception.garage;

public class Car extends Vehicle {
    @Override
    public void repair() {
        System.out.println("car is repaired");
    }
}
