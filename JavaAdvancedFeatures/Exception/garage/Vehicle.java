package SDA.sdacademy.JavaAdvancedFeatures.Exception.garage;

public abstract class Vehicle {
    public abstract void repair();
}
