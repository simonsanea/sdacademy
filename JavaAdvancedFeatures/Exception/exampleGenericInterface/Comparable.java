package SDA.sdacademy.JavaAdvancedFeatures.Exception.exampleGenericInterface;

public interface Comparable<T> {
    public int compareTo(T o);
}
