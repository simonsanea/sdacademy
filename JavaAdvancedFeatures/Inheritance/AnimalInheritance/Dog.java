package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.AnimalInheritance;

public class Dog extends Animal {
    @Override
    public void eat(int kilograms) {
        System.out.println("Animal is eating " + kilograms);
    }
}
