package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.AnimalInheritance;

public class Animal {

    public void yieldVoice() {

    }

    public void eat() {
        System.out.println("Animal is eating");
    }

    public void eat(String food) {
        System.out.println("Animal is eating " + food);
    }

    public void eat(int kilograms) {
        System.out.println("Animal is eating " + kilograms + " meat");
    }

    public void eat(int kilograms, String food) {
        System.out.println("Animal is eating " + kilograms + " of " + food);
    }
}
