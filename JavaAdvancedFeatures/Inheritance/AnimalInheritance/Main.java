package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.AnimalInheritance;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();
        animal.eat();
        animal.eat("Bone");
        Dog dog = new Dog();
        dog.eat(12);
        dog.eat("Breed");
    }
}
