package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.CarInheritance;

public class Vehicle {
    private int maxSpeed;

    public Vehicle(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String toString() {
        return "Fields values maxSpeed= " + maxSpeed;
    }

    public int getMaxSpeed() {
        return this.maxSpeed;
    }
}
