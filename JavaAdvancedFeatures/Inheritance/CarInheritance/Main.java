package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.CarInheritance;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car(130, false);
        Vehicle myVehicle = new Car(210, true);
        Vehicle myVehicle2 = new Vehicle(85);
        Vehicle myCarVehicle = new Car(85, true);

        System.out.println(myVehicle2.toString());
        System.out.println(myCarVehicle.toString());
        System.out.println(myCar.getMaxSpeed() + " " + myCar.isConvertible());
        System.out.println(myVehicle);
    }
}
