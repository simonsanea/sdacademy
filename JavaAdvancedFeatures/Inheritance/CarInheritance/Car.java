package SDA.sdacademy.JavaAdvancedFeatures.Inheritance.CarInheritance;

public class Car extends Vehicle {

    private boolean convertible;

    public Car(int maxSpeed, boolean convertible) {
        super(maxSpeed);
        this.convertible = convertible;
    }

    @Override
    public String toString() {
        return super.toString() +
                " ,convertible= " + convertible;
    }

    public boolean isConvertible() {
        return this.convertible;
    }
}
