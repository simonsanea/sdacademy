package SDA.sdacademy.JavaAdvancedFeatures.Casting;

public class Truck extends Car {
    private String licenseNumber;

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @Override
    public void start(){
        System.out.println("TRUCK start");
    }
    @Override
    public void stop (){
        System.out.println("TRUCK stop");
    }
}
