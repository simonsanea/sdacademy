package SDA.sdacademy.JavaAdvancedFeatures.Casting;

public class Car {
    private int speed;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void start(){
        System.out.println("CAR start");
    }
    public void stop (){
        System.out.println("CAR stop");
    }
}
