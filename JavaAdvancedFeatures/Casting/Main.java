package SDA.sdacademy.JavaAdvancedFeatures.Casting;

public class Main {
    public static void main(String[] args) {
        /**up-casting*/
        Car c1 =  (Truck) new Truck();
        c1.start();
        Truck t1 = (Truck) new IcecreamTruck();


        /**down-casting*/
        Car c2 = new IcecreamTruck();
        IcecreamTruck icecreamTruck1 = (IcecreamTruck) c2;
        Truck t2 = (Truck) c2;


        /**down-casting that will fail*/
        Car c = new Car();
        Truck t3 = (Truck) c;

    }
}
