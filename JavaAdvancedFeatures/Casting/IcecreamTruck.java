package SDA.sdacademy.JavaAdvancedFeatures.Casting;

public class IcecreamTruck extends Truck {
    private boolean hasIcecream;

    public boolean isHasIcecream() {
        return hasIcecream;
    }

    public void setHasIcecream(boolean hasIcecream) {
        this.hasIcecream = hasIcecream;
    }
    @Override
    public void start(){
        System.out.println("ICECREAMTRUCK start");
    }
    @Override
    public void stop (){
        System.out.println("ICECREAMTRUCK stop");
    }
}
