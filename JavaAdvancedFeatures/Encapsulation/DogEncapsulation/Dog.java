package SDA.sdacademy.JavaAdvancedFeatures.Encapsulation.DogEncapsulation;

public class Dog {
    public static void main(String[] args) {
        DogMethod dogMethod = new DogMethod("Male", "Labrador");
        DogMethod dogMethod1 = new DogMethod("Rex", 7, "Male", "Doberman", 10.06);

        System.out.println(dogMethod);
        System.out.println(dogMethod1);
    }
}
