package SDA.sdacademy.JavaAdvancedFeatures.Encapsulation.DogEncapsulation;

public class DogMethod {
    private String name;
    private int age;
    private String gender;
    private String race;
    private double weight;

    public DogMethod(String gender, String race) {
        this("Ben",3,"Male","Doberman",10.90);
        this.gender = gender;
        this.race = race;
    }

    public DogMethod(String name, int age, String gender, String race, double weight) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.race = race;
        this.weight = weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(double weight) {
        if (weight < 0)
            System.out.println("Inserted wrong weight please try again");
        else
            this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getRace() {
        return race;
    }

    public double getWeight() {
        return weight;
    }
}
