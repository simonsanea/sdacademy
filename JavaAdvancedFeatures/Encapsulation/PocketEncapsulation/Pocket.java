package SDA.sdacademy.JavaAdvancedFeatures.Encapsulation.PocketEncapsulation;

public class Pocket {
    public static void main(String[] args) {
        PocketMethod pocketMethod = new PocketMethod();
        PocketMethod pocketMethod1 = new PocketMethod();
        PocketMethod pocketMethod2 = new PocketMethod();
        pocketMethod2.setMoney(1009);
        pocketMethod.setMoney(3500);
        pocketMethod1.setMoney(5);
        System.out.println(pocketMethod.getMoney());
        System.out.println(pocketMethod1.getMoney());
        System.out.println(pocketMethod2.getMoney());
    }
}
