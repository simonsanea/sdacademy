package SDA.sdacademy.JavaAdvancedFeatures.Encapsulation.PocketEncapsulation;

public class PocketMethod {
    private int money;

    public void setMoney(int money) {
        if (money < 0 || money > 3000) {
            System.out.println("I don't have enough space in my pocket for as much money!");
        } else {
            this.money = this.money + money;
        }
    }

    public int getMoney() {
        return this.money;
    }

    public int getMoneyFromPocket() {
        if (money <= 10) {
            return this.money = 0;
        } else {
            return this.money;
        }
    }
}
