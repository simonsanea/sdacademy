package SDA.sdacademy.WorkInClass.Alghoritm;

import java.util.Scanner;

public class FibonacciSequence {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a = 0;
        int b = 1;
        int aux;
        int count = 0;
        while (count <= n) {
            System.out.print(a + " ");
            count++;
            aux = b;
            b = a + b;
            a = aux;
        }
    }
}
