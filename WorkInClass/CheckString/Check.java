package SDA.sdacademy.WorkInClass.CheckString;

import java.util.Scanner;

public class Check {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter first string");
        String first = in.nextLine();
        System.out.println("Please enter second string");
        String second = in.nextLine();
        Check.check(first, second);
        boolean areEquals = Check.check(first, second);
        System.out.println(second + " contains in " + first + " : " + areEquals);
    }

    public static boolean check(String first, String second) {
        boolean isFound;
        if (first.contains(second)) {
            isFound = true;
        } else
            isFound = false;
        return isFound;
    }
}
