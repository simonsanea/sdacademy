package SDA.sdacademy.WorkInClass.HomeDaily;

public class Person {
    String firstName;
    String lastName;

    public static void sleep() {
        System.out.println("Person is sleeping");
    }

    public static void eat() {
        System.out.println("Person is eating");
    }

    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
