package SDA.sdacademy.WorkInClass.HomeDaily;

public class Test {
    public static void main(String[] args) {
        Person person = new Person("Ion", "Popescu");
        System.out.println(person.firstName + " " + person.lastName);
        Person.eat();
        Person.sleep();
    }
}
