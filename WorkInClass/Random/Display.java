package SDA.sdacademy.WorkInClass.Random;

import java.util.Random;

public class Display {
    public static void main(String[] args) {
        Random random = new Random();
        int numberRandom = random.nextInt(99);
        int[] myArray = new int[10];
        for (int i = 0; i < myArray.length; i++) {
            numberRandom = random.nextInt(99);
            myArray[i] = numberRandom;
        }
        System.out.println();
        display(myArray);
        evenNumber(myArray);
        oddNumber(myArray);
    }

    public static void display(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            System.out.print(myArray[i] + " ");
        }
    }

    public static void oddNumber(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] % 2 == 0) {
                System.out.print(myArray[i] + " ");
            }
        }
    }

    public static void evenNumber(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] % 2 != 0) {
                System.out.print(myArray[i] + " ");
            }
        }
    }
}
