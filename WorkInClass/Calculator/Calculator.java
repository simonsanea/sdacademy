package SDA.sdacademy.WorkInClass.Calculator;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int num2 = in.nextInt();
        System.out.println(Calculator.add(num, num2));
        System.out.println(Calculator.sub(num, num2));
    }

    public static int add(int x, int y) {
        return x + y;
    }

    public static double sub(int x, int y) {
        return x / y;
    }
}
